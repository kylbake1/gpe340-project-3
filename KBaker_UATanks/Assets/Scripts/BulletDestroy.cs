﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour
{
    public TankData data; //  This will call the TankData script can rename it data
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, data.bulletDestroyTime); // At the start of the game, The bullet will begin to get destroyed by using the bulletDestroyTime from the inspector
    }

    private void OnCollisionEnter(Collision bullet) // This is OnCollisionEnter that is called bullet for ease
    {
        if (bullet.gameObject.tag == "Wall") // if the bullet makes contact with the wall tagged "Wall"
        {
            Destroy(this.gameObject);// Destroy the bullet
        }
    }
}
