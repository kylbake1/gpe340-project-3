﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    // This section is public and customizable in the inspector for the designers
    public Transform[] waypoints;        // This will make an array of waypoints in the inspector for the designer to place waypoints
    public TankMotor motor;              // this will use the TankMotor script and call it motor
    public TankData data;               // this will use the TankData script and call it data
    public float closeEnough = 1.0f;     // this will allow the AI to get close enough to the waypoint because it wont be absolutely on point
    public float fleeDistance = 1.0f;    // this will cause the AI to flee when were 1 unit away

    public enum AIStates { Chase, Flee, Patrol, Sense}       // this is the enumerator of AIStates that the designer can select from
    public AIStates aIStates = AIStates.Patrol;              // this will start the AI on the patrol state by default

    public enum LoopType { Loop, PingPong };         // this is the enumerator of patrol settings that the designer can change
    public LoopType loopType = LoopType.Loop;        // the AI will begin to loop first if we done select one of the two

    public enum Sense { Idle, Chase, LookAround, GoHome }        // this is the enumerator of senses for the AI that the designer can use and for the AI to interchage
    public Sense sense = Sense.LookAround;                       // this uses the Sense script called sense and it will be equal to lookAround from Sense
    public AIStates currentState;                                // This will show the current state of the AI

    public Transform target;             // uses the Transform component and names it target
    public float avoidanceTime = 2.0f;   // this will be the time it takes to avoid walls

    // This section is private and is not customizable to the designers
    private bool isPatrolForward = true;         // this will move the AI forward when true
    private int currentWaypoint = 0;             // the current point the AI is on so it can begin moving down the list in the array
    private Transform tf;                        // transform is called tf
    private float exitTime;                      // this is the time to exit a state
    private int avoidanceStage = 0;              // this will interchange states for the AI
    private Vector3 homePoint;                   // the homepoint for the AI to remember
    private Vector3 goalPoint;                   // this is the goal as to where the AI wants to go
    private AISenses senses;                     // this uses AISenses and calls it senses

    public void Awake() // before the game starts, all of this will happen
    {
        tf = gameObject.GetComponent<Transform>();       // tf will now be automatically filles with the transform component
        data = GetComponent<TankData>();                 // data will now be filled with the TankData script on the GameObject
        motor = GetComponent<TankMotor>();               // motor will now be filled with the TankMotor script on the GameObject
        senses = GetComponent<AISenses>();               // senses will now be filled with the AISenses script on the GameObject
        homePoint = tf.position;                         // This will set the current postion of the AI to 0,0,0 for home

    }

    // Update is called once per frame
    void Update()
    {
        if (aIStates == AIStates.Chase)      // if the designer chooses the chase state 
        {
            if (avoidanceStage != 0)         // if the avoidance int is not 0 for obstacle avoidance
            {
                DoAvoidance();               // the AI will perform the DoAvoidance function
            }
            else                             // if it is 0,
            {
                Chase();                     // then the AI will perform the chase function
            }
        }

        if (aIStates == AIStates.Flee)       // if the designer chooses the flee state 
        {
            if( avoidanceStage != 0)         // if the avoidance int is not 0 for obstacle avoidance
            {
                DoAvoidance();               // the AI will perform the DoAvoidance function
            }
            else                             // if it is 0,
            {
                Flee();                      // then the AI will perform the flee function
            }
        }

        if(aIStates == AIStates.Patrol)      // if the designer chooses the patrol state 
        {
            if(avoidanceStage != 0)          // if the avoidance int is not 0 for obstacle avoidance
            {
                DoAvoidance();               // the AI will perform the DoAvoidance function
            }
            else                             // if it is 0,
            {
                Patrol();                    // then the AI will perform the patrol function
            }
        }
    }

    public void LoopLoop()                           // 1 of 2 patrol states to choose from
    {
        if (currentWaypoint < waypoints.Length - 1)  // Advance to the next waypoint, if we are still in range
        {
            currentWaypoint++;                       // add one to the current waypoint
        }
        else 
        {  
            currentWaypoint = 0;                     // head back to waypoint 0
        }
    }

    public void LoopPingPong()                           // this is 2 of 2 patrol states to choose from
    {
        if (isPatrolForward)                             // if this is true
        {
            if (currentWaypoint < waypoints.Length - 1)  // Advance to the next waypoint, if we are still in range
            {
                currentWaypoint++;                       // add 1 to the current waypoint list
            }
            else
            {
                isPatrolForward = false;                 // then the direction will be in reverse and
                currentWaypoint--;                       // current waypoint will go back down rather the up 1
            }
        }
        else
        {
            if (currentWaypoint > 0)                     // if the current waypoint is less than 0
            {
                currentWaypoint--;                       // current waypoint will now move down to the next waypoint 
            }
            else
            {
                isPatrolForward = true;                  // otherwise, the AI will move forward
                currentWaypoint++;                       // towards the next postive waypoint up the list
            }
        }
    }

    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Rotate left
            motor.Rotate(-1 * data.turnSpeed);

            // If I can now move forward, move to stage 2!
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                // Set the number of seconds we will stay in Stage2
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            // if we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                // Subtract from our timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                // If we have moved long enough, return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Otherwise, we can't move forward, so back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    public bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hit something...
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            // ... and if what we hit is not the player...
            if (!hit.collider.CompareTag("Player"))
            {
                // ... then we can't move
                return false;
            }
        }
        // otherwise, we can move, so return true
        return true;
    }

    public void Chase()
    {
        motor.RotateTowards(target.position, data.turnSpeed);
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            avoidanceStage = 1;
        }
    }

    public void Patrol()
    {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
        {
            // Do nothing!
        }
        else
        {
            // Move forward
            motor.Move(data.moveSpeed);
        }

        // If we are close to the waypoint,
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            switch (loopType)
            {
                case LoopType.Loop:
                    if (loopType == LoopType.Loop)
                    {
                        LoopLoop();
                    }
                    break;

                case LoopType.PingPong:
                    if (loopType == LoopType.PingPong)
                    {
                        LoopPingPong();
                    }
                    break;
            }
        }
    }

    public void Flee()
    {
        // The vector from ai to target is target position minus our position
        Vector3 vectorToTarget = target.position - tf.position;

        // we can flip the vector by -1 to get the vector AWAY from our target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // now, we can normalize that vector to give it a magnitute of 1
        vectorAwayFromTarget.Normalize();

        // a normalized vector can be multiplied by a length to make a vector of that length
        vectorAwayFromTarget *= fleeDistance;

        // we can find the position in space we want to move to by adding our vector away from our AI to our AI's position
        // this gives us a point that is "that vector away" from our current position
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.turnSpeed);
        motor.Move(data.moveSpeed);
    }

    public void Idle()
    {
        // Do Nothing
    }

    public void GoHome()
    {
        goalPoint = homePoint;
        MoveTowards(goalPoint);
    }

    public void LookAround()
    {
        Turn(true);
    }

    public void MoveTowards(Vector3 target)
    {
        if (Vector3.Distance(tf.position, target) > closeEnough)
        {
            // Look at target
            Vector3 vectorToTarget = target - tf.position;
            tf.right = vectorToTarget;

            // Move Forward
            Move(tf.right);
        }
    }

    public void Move(Vector3 direction)
    {
        // Move in the direction passed in, at speed "moveSpeed"
        tf.position += (direction.normalized * data.moveSpeed * Time.deltaTime);
    }

    public void Turn(bool isTurnClockwise)
    {
        // Rotate based on turnSpeed and direction we are turning
        if (isTurnClockwise)
        {
            tf.Rotate(0, 0, data.turnSpeed * Time.deltaTime);
        }
        else
        {
            tf.Rotate(0, 0, -data.turnSpeed * Time.deltaTime);
        }
    }
}
