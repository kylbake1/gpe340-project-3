﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
       
    // run before any start() functions run
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("Error: There can only be one GameManager.");
            Destroy(gameObject);
        }
    }

    // This is for the RandomSpawner
    public GameObject enemyToSpread; // creates a gameobject holder in the inspector
    public GameObject playerToSpread; // creates a gameobject holder in the inspector

    public int numEnemiesToSpawn; // creates an int in the inspector that the designer can set 

    public float itemXSpread; // creates a float that will serve as the x axis of a grid
    public float itemYSpread; // creates a float that will serve as the y axis of a grid
    public float itemZSpread; // creates a float that will serve as the z axis of a grid

    // This is for PickItem

    public GameObject[] AisToPickFrom; // this array will hold the AI's that were created in the game for spawning

    // This is for the Tile Map Generator
    public int rows; // this int will contain the number of rows our tiles will spawn in
    public int cols; // this int will contain the number of columns our tiles will spawn in
    public int mapSeed; // this contains a int that will determine the seed of the game that we can always choose for the same map
    public bool isMapOfTheDay; // this will change the map every time the time of the day changes for a new map of the day

    private float roomWidth = 50.0f; // this is the width of the room and it is private
    private float roomHeight = 50.0f; // this is the height of the room and it is private
    private Room[,] grid; // thei grid will contain the coordiates for each room 
    public GameObject[] gridPrefabs; // creates a prefab that we can place the different room tiles in
    

    // Use this for initialization
    void Start()
    {
        // Generate Grid
        GenerateGrid(); // this wil call the function
        if (isMapOfTheDay) // if the bool is set to true
        {
            mapSeed = DateToInt(DateTime.Now.Date); // the random seed of the day will activate based on todays date
        }
    }

    public void GenerateGrid() 
    {
        UnityEngine.Random.InitState(mapSeed); // this will set the random seed for us using unitys built in random
        UnityEngine.Random.InitState(DateToInt(DateTime.Now)); // This will be for the seed of the day

        grid = new Room[rows, cols]; // this will create a new grid everytime and get rid of the old grid before the game starts


        for (int i = 0; i < rows; i++) // this is the For each grid row
        {
            
            for (int j = 0; j < cols; j++) // for each column in that row
            {
                float xPosition = roomWidth * j; // this creates a float and will set it equal to the width of the room that we determined is 50.0f and creates more rooms which is the value j
                float zPosition = roomHeight * i; // this creates a float and will set it equal to the height of the room that is also determined as 50.0f and creates more room adjacent to it with the value i
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition); // the new positions will be adjacent to each other

                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject; // creates a new gameobject and it will clone a prefab of the rooms we made in a new location that wont be effected in rotation
                tempRoomObj.transform.parent = this.transform; // Set its parent

                tempRoomObj.name = "Room_" + j + "," + i; // This will name the new room for us with the grid number it's placed in
               
                Room tempRoom = tempRoomObj.GetComponent<Room>(); // Get the room object

                // Open the doors
                // If we are on the bottom row, open the north door
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (i == rows - 1)
                {
                    // Otherwise, if we are on the top row, open the south door
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    // Otherwise, we are in the middle, so open both doors
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                // If we are on the first column, open the east door
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (j == cols - 1)
                {
                    // Otherwise, if we are on the last column row, open the west door
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    // Otherwise, we are in the middle, so open both doors
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
                // Save it to the grid array
                grid[j, i] = tempRoom;
            }
        }
    }

    // Returns a random room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public int DateToInt(DateTime dateToUse)
    {
        // Add our date up and return it
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }
}
