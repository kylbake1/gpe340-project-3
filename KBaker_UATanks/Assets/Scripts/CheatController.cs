﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : MonoBehaviour
{
    public PowerupController powCon;
    public Powerup cheatPowerup;

    // Start is called before the first frame update
    void Start()
    {
        if(powCon == null)
        {
            powCon = gameObject.GetComponent<PowerupController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.H)&& Input.GetKey(KeyCode.U) && Input.GetKey(KeyCode.E))
        {
            powCon.Add(cheatPowerup);
        }
    }
}
