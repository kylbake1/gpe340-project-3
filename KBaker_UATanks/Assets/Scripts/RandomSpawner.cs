﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public static GameManager instance;
    
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < GameManager.instance.numEnemiesToSpawn; i++) // this will randomly spawn in the number of enemies that the designer chooses 
        {
            SpreadItem(); // this will run the fuction as many times as the designer has set to spawn in the enemies per tile
        }        
    }
    public void SpreadItem()
    {
        Vector3 randPosition = new Vector3(Random.Range(-GameManager.instance.itemXSpread, GameManager.instance.itemXSpread), Random.Range(-GameManager.instance.itemYSpread, GameManager.instance.itemYSpread), Random.Range(-GameManager.instance.itemZSpread, GameManager.instance.itemZSpread)) + transform.position; // this will spawn the enemy in a random location within the set coordiates from the designer
        GameObject clone = Instantiate(GameManager.instance.enemyToSpread, randPosition, Quaternion.identity);  // this will create a clone of the prefab and spawn in a AI from the index, put it in a location, and keep the current rotations 
    }
}
